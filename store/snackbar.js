import { SnackbarProgrammatic as Snackbar } from 'buefy'

export const state = () => ({})

export const actions = {
  setSnackbar (context, { text, type = 'is-success', position = 'is-bottom' }) {
    Snackbar.open({
      message: text,
      actionText: 'Close',
      type,
      position
    })
  }
}
